#
# Be sure to run `pod lib lint skilaichat.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'skilaichat'
  s.version          = '0.1.1'
  s.summary          = 'skilaichat contact.'
  s.swift_version    = '4.2'
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
'skilaichat contact'.
                       DESC

  s.homepage         = 'https://github.com/KOSURUUDAYSAIKUMAR/skilaichat'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'udaysai.k@kairostech.com' => 'kosuruudaysaikumar@gmail.com' }
  s.source           = { :git => 'https://github.com/KOSURUUDAYSAIKUMAR/skilaichat.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.2'

  s.source_files = 'skilaichat/Classes/**/*'
  
  # s.resource_bundles = {
  # 'skilaichat' => ['skilaichat/Assets/*.png']
  # }

  # s.public_header_files = 'skilaichat/Classes/**/*.h'
   s.frameworks = 'UIKit', 'MapKit', 'Speech'
   s.dependency 'DGActivityIndicatorView'
   s.dependency 'CropViewController'
   s.dependency 'IQKeyboardManagerSwift'
   s.dependency 'ImageAlertAction'
end
