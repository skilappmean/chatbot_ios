//
//  ViewController.swift
//  skilaichat
//
//  Created by udaysai.k@kairostech.com on 06/27/2019.
//  Copyright (c) 2019 udaysai.k@kairostech.com. All rights reserved.
//

import UIKit
import skilaichat
//import MessengerKit

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate {

    var skilvoice = SkilVoiceMessage()
    
    @IBOutlet var chatBtnOutlet: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        skilvoice.requestTranscribePermissions()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func chattingButton(_ sender: UIButton)
    {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let popupVC = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        popupVC.modalPresentationStyle = .popover //.overCurrentContext
        popupVC.preferredContentSize = CGSize(width: UIScreen.main.bounds.size.width * 0.8, height: UIScreen.main.bounds.size.height * 0.8) // CGSize(width: 300, height: 550) //
        popupVC.view.backgroundColor = .clear
        let pVC = popupVC.popoverPresentationController
        pVC?.delegate = self
        pVC?.sourceView = chatBtnOutlet
        pVC?.sourceRect = chatBtnOutlet.bounds
        pVC?.permittedArrowDirections = .init(rawValue: 0)
        present(popupVC, animated: true, completion: nil)

    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

