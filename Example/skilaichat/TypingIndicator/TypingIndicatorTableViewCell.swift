//
//  TypingIndicatorTableViewCell.swift
//  skilaichat_Example
//
//  Created by Uday  on 09/07/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import DGActivityIndicatorView



class TypingIndicatorTableViewCell: UITableViewCell {

    @IBOutlet var TypingView: UIView!
    let activityIndicatorView = DGActivityIndicatorView(type:DGActivityIndicatorAnimationType.ballPulse, tintColor: UIColor.lightGray, size: 30.0)
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        TypingView.layer.cornerRadius = 15
        activityIndicatorView!.frame = CGRect(x: 0.0, y: 0.0, width: 50.0, height: 30.0)
        activityIndicatorView?.backgroundColor = UIColor.clear
        TypingView.addSubview(activityIndicatorView!)
//        TypingView.addShadowWith(shadowPath: UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: 50, height: 30), cornerRadius: 15).cgPath, shadowColor: UIColor.black.withAlphaComponent(0.1).cgColor, shadowOpacity: 1.0, shadowRadius: 10.0, shadowOffset: CGSize(width: 3, height: 3))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
