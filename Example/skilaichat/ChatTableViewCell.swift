//
//  ChatTableViewCell.swift
//  skilaichat_Example
//
//  Created by Uday  on 01/07/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet var receiverImage: UIImageView!
    @IBOutlet var receivername: UILabel!
    @IBOutlet var receiverText: UILabel!
    
    @IBOutlet var chatTextView: UITextView!
    @IBOutlet var senderText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
