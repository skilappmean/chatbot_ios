//
//  ImageUploadTableViewCell.swift
//  skilaichat_Example
//
//  Created by Uday  on 09/07/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit

class ImageUploadTableViewCell: UITableViewCell {

    @IBOutlet var uploadImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
