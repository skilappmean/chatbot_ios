//
//  ChatViewController.swift
//  skilaichat_Example
//
//  Created by Uday  on 28/06/19.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import UIKit
import Speech
import skilaichat

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, SFSpeechRecognizerDelegate
{

    @IBOutlet var chatTableView: UITableView!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var middleView: UIView!
    
    @IBOutlet var waveFormImageView: UIImageView!
    @IBOutlet var voiceBtnOutlet: UIButton!
    @IBOutlet var bottomViewContraits: NSLayoutConstraint!
    @IBOutlet var bottomView: UIView!
    
    var arrTextData: [[String: Any]] = [["message":"Hey there! You're back 👋 What's up?","isMe":"0","date":"12 May,2018","sender":"Lisa", "image" : ""]]
    
    var receiverRandomArray = ["Can you please rate on Date & Time of the event?", "How mostly would you prefer this location for the event on a scale of 0 to 5?", " Please share your feedback to improve our community events.", "I am Lisa"]
    
    var speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: Language.instance.setlanguage()))!
    
    var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    
    var recognitionTask: SFSpeechRecognitionTask?
    
    let audioEngine = AVAudioEngine()
    var textViewPlaceHolder = "Reply to Lisa from Skil.ai"
    var shouldShowTypingCell = false
    var imageViewHeight = NSLayoutConstraint()
    var imageRatioWidth = CGFloat()
    
    override func viewDidAppear(_ animated: Bool) {
        
        speechRecognizer.delegate = self
        requestAuthorization()
        
        
    }
    
    // MARK: - Table view data source
    func numberOfSections(in tableView: UITableView) -> Int
    {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete implementation, return the number of rows
        return arrTextData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Configure the cell...
        let msg = arrTextData[indexPath.row]
        var reuseIdentifier = "ChatDetailCellTextSend"
        if  "\(msg["isMe"] ?? "")" == "0"
        {
            reuseIdentifier = "ChatDetailCellTextReceive"
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ChatTableViewCell
            cell.chatTextView.layer.cornerRadius = 8
            cell.receivername?.text = msg["sender"] as? String
            cell.chatTextView?.text = msg["message"] as? String
            cell.receiverImage?.image = UIImage(named: "bot icon1")
            return cell
        }
        
        if "\(msg["isMe"] ?? "")" == "1" && msg["message"] as? String != ""
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ChatTableViewCell
            cell.chatTextView.layer.cornerRadius = 8
            cell.receivername?.text = msg["sender"] as? String
            cell.chatTextView?.text = msg["message"] as? String
            cell.receiverImage?.image = UIImage(named: "bot icon1")//?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            return cell
        }
        if "\(msg["isMe"] ?? "")" == "1" &&  msg["message"] as? String == ""
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageUploadTableViewCell", for: indexPath) as! ImageUploadTableViewCell
            cell.uploadImage.image =  (msg["image"] as! UIImage)
            chatTableView.beginUpdates()
          
            tableView.endUpdates()
            return cell
        }
        if shouldShowTypingCell == true
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TypingIndicatorTableViewCell", for: indexPath) as! TypingIndicatorTableViewCell
            cell.activityIndicatorView?.startAnimating()
            return cell
        }
        else
        {
            return ChatTableViewCell()
        }
//        cell?.viewContainer?.layer.cornerRadius = 10.0
//        cell?.viewContainer?.backgroundColor = bgColor
        
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let msg = arrTextData[indexPath.row]
        if "\(msg["isMe"] ?? "")" == "1" &&  msg["message"] as? String == ""
        {
            let imageRatio = (msg["image"] as! UIImage).getImageRatio()
            return tableView.frame.width / imageRatio
        }
        else
        {
            return tableView.estimatedRowHeight
        }
    }
    
    var attachment = AttachmentHandler()
    @IBAction func attachButton(_ sender: Any)
    {
        //USAGE
         AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
         AttachmentHandler.shared.imagePickedBlock = { (image) in
         /* get your image here */
            self.arrTextData.append(["message":"", "isMe":"1","date":"12 May,2018","sender":"User", "image" : image])
            self.chatTableView.reloadData()
         }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        let lastRowIndex = tableView.numberOfRows(inSection: 0)
        if indexPath.row == lastRowIndex - 1
        {
            tableView.scrollToBottom(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell is TypingIndicatorTableViewCell
        {
             (cell as! TypingIndicatorTableViewCell).activityIndicatorView?.stopAnimating()
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        messageTextView.text = textViewPlaceHolder
        messageTextView.textColor = UIColor.lightGray
        messageTextView.font = UIFont(name: "verdana", size: 13.0)
        messageTextView.returnKeyType = .done
        messageTextView.delegate = self
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.register(UINib(nibName: "TypingIndicatorTableViewCell", bundle: nil), forCellReuseIdentifier: "TypingIndicatorTableViewCell")
        chatTableView.register(UINib(nibName: "ImageUploadTableViewCell", bundle: nil), forCellReuseIdentifier: "ImageUploadTableViewCell")
        self.textViewSettings()
        self.tableViewSettings()
        self.keyboardSettings()
        self.waveFormImageView.isHidden = true
        voiceBool = false
    }
    
    //Settings
    func keyboardSettings()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        let swipeDown = UISwipeGestureRecognizer(target: self.view , action : #selector(UIView.endEditing(_:)))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if (text == "\n")
        {
            messageTextView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //MARK:- UITextViewDelegates
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if textView.text == textViewPlaceHolder
        {
            textView.text = ""
            textView.textColor = UIColor.black
            //            textView.font = UIFont(name: "verdana", size: 18.0)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if textView.text == ""
        {
            textView.text = textViewPlaceHolder
            textView.textColor = UIColor.lightGray
            //            textView.font = UIFont(name: "verdana", size: 13.0)
        }
        else
        {
            messageTextView.text = textView.text
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
    
    func textViewSettings ()
    {
        self.messageTextView.layer.borderWidth = CGFloat(0.5)
        self.messageTextView.layer.borderColor = UIColor.darkGray.cgColor
        self.messageTextView.layer.cornerRadius = CGFloat(13)
    }
    
    @IBAction func closeBtnHandler(_ sender: UIButton)
    {
        self.dismiss(animated: true) {
            print("Dismiss popover viewcontroller")
        }
    }
    
    func tableViewSettings()
    {
       // self.chatTableView.rowHeight = UITableView.automaticDimension
      //  self.chatTableView.estimatedRowHeight = 140
        self.chatTableView.delegate = self
        self.chatTableView.dataSource = self
    }
    
    //Keyboard Configure
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue{
                if let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue{
                    
                    let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
                    let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
                    let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
                    
                    if (endFrame.origin.y) >= UIScreen.main.bounds.size.height {
                        self.bottomViewContraits?.constant = 0.0
                    } else {
                        self.bottomViewContraits?.constant = endFrame.size.height
                    }
                    
                    
                    UIView.animate(withDuration: duration,
                                   delay: TimeInterval(0),
                                   options: animationCurve,
                                   animations: { self.view.layoutIfNeeded() },
                                   completion: nil)
                }
            }
        }
    }
    
    @IBAction func sendBtn(_ sender: Any)
    {
        if !messageTextView.text.trim().isEqual("")
        {
            arrTextData.append(["message": messageTextView.text, "isMe":"1","date":"12 May,2018","sender":"User", "image" : ""])
            arrTextData.append(["message": receiverRandomArray.randomElement()!, "isMe":"0","date":"12 May,2018","sender":"Lisa", "image" : ""])
            messageTextView.text = ""
            chatTableView.reloadData()
            messageTextView.text = textViewPlaceHolder
            messageTextView.textColor = UIColor.lightGray
            messageTextView.font = UIFont(name: "verdana", size: 13.0)
            messageTextView.returnKeyType = .done
            messageTextView.delegate = self
            shouldShowTypingCell = true
            if voiceBtnOutlet.isEnabled == true
            {
               self.waveFormImageView.isHidden = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var voiceBool = Bool()
    @IBAction func voice(_ sender: UIButton)
    {
        if voiceBool == false
        {
            try! startRecording()
            self.waveFormImageView.image = UIImage.gif(name: "speech")
            sender.setImage(UIImage(named: "chat.png"), for: .normal)
            self.waveFormImageView.isHidden = false
            voiceBool = true
            print("start recording")
        }
        else if voiceBool == true, let _ = UIImage(named:"chat.png")
        {
            print("stop recording")
            voiceBool = false
            audioEngine.stop()
            recognitionRequest?.endAudio()
            
            self.audioEngine.inputNode.removeTap(onBus: 0)
            self.recognitionTask?.cancel()
            self.waveFormImageView.isHidden = true
            sender.setImage(UIImage(named: "microphone icon.png"), for: .normal)
        }
    }
}



extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    }
}

extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if (rows > 0){
            self.scrollToRow(at: NSIndexPath(row: rows - 1, section: sections - 1) as IndexPath, at: .bottom, animated: true)
        }
    }
}

extension UIImage {
    func getImageRatio() -> CGFloat {
        let imageRatio = CGFloat(self.size.width / self.size.height)
        return imageRatio
    }
}
