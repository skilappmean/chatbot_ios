//
//  SpeachToText.swift
//  SpeechToText
//
//  Created by claudio Cavalli on 17/01/2019.
//  Copyright © 2019 claudio Cavalli. All rights reserved.
//

import UIKit
import Speech
import skilaichat

extension ChatViewController
{
    
    func startRecording() throws {
        
        if recognitionTask != nil {
            recognitionTask?.cancel()
            recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .measurement, options: .interruptSpokenAudioAndMixWithOthers)
            try audioSession.setActive(true)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        
        guard let recognitionRequest = recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in
            var isFinal = false
            if(result?.bestTranscription.formattedString != nil){
                DispatchQueue.main.async {
                    self.messageTextView.text =  (result?.bestTranscription.formattedString)!
                    self.messageTextView.textColor = UIColor.black
                }
            }
            
            if(result?.isFinal != nil)
            {  isFinal = (result?.isFinal)!}
            
            if isFinal || error != nil {
                let command = result?.bestTranscription.formattedString
                
                if command != nil {
                    self.audioEngine.inputNode.removeTap(onBus: 0)
                    
                    self.audioEngine.stop()
                    
                    self.recognitionRequest?.endAudio()
                    self.recognitionTask?.cancel()
                    
                }
                
            }
            if error != nil
            {
                print("Error for voice", error!.localizedDescription)
            }
        })
        
        var recordingFormat = inputNode.inputFormat(forBus: 0)
        var trueFormat: AVAudioFormat!
        if recordingFormat.sampleRate == 0 {
            trueFormat = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)
        } else {
            trueFormat = recordingFormat
        }
        print("installing tap: \(recordingFormat.sampleRate) -- \(recordingFormat.channelCount)")
        recordingFormat = AVAudioFormat(standardFormatWithSampleRate: 44100, channels: 1)!
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: trueFormat) { [unowned self] (buffer, when) in
            self.recognitionRequest?.append(buffer)
            print("in tap completion")
            let sampleData = UnsafeBufferPointer(start: buffer.floatChannelData![0], count: Int(buffer.frameLength))
        }
        audioEngine.prepare()
        try audioEngine.start()
    }
    
    
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
          //  voiceBtnOutlet.isEnabled = true
            print("Start Recording", available)
        } else {
          //  voiceBtnOutlet.isEnabled = false
            print("Recognition not available", available)
        }
    }
    
}
