# skilaichat

[![CI Status](https://img.shields.io/travis/udaysai.k@kairostech.com/skilaichat.svg?style=flat)](https://travis-ci.org/udaysai.k@kairostech.com/skilaichat)
[![Version](https://img.shields.io/cocoapods/v/skilaichat.svg?style=flat)](https://cocoapods.org/pods/skilaichat)
[![License](https://img.shields.io/cocoapods/l/skilaichat.svg?style=flat)](https://cocoapods.org/pods/skilaichat)
[![Platform](https://img.shields.io/cocoapods/p/skilaichat.svg?style=flat)](https://cocoapods.org/pods/skilaichat)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

Once open example project, drag and drop the custom styele folder and give the UIViewController named as CustomStyleViewController. 

## Requirements

## Installation

skilaichat is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'skilaichat'
pod 'MessengerKit', :git => 'https://github.com/steve228uk/MessengerKit.git'
```

## Author

udaysai.k@kairostech.com, kosuruudaysaikumar@gmail.com

## License

skilaichat is available under the MIT license. See the LICENSE file for more info.
